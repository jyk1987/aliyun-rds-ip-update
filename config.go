package main

type Config struct {
	AccessKeyID     string
	AccessKeySecret string
	InstanceId      string
	IpGroupName     string
	RegionID        string
}
